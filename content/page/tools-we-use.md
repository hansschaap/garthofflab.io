---
title: The tools we use
type: docs
---
# Tools we use

Like every company we also use tools to perform our jobs. Depending on our job we need different tools. However we all need the same basic tools to do some basic tasks. To prevent having too many different tools there is a basic collection of tools we use in Curious.

## Company wide software tools

<u>Tools we 'all' use are:</u>

**Office 365 (Outlook, Word, Excel, PowerPoint, ... )**

**Microsoft Teams** We use Teams to communicate to each other. For all of our video calls (internal) we use Teams. Also speaking without video is done mainly via Teams.

**Slack** chat communication is also still done via Slack. Remember that we don't have a paid version and that messages will disappear after limited time.

<u>Tools all remote working colleagues use:</u>

**Forticlient VPN** To be able to work remote you might need access to some systems which can only be reached using VPN. Our solution is Forticlient in combination with an account that can be request and will be created by our [IT partner]().

<u>Software development tools:</u>

**Gitlab** We use GitLab as our vision control solution for all of our software. If you are a developer you will need to request access to the projects you will work on.

**CircleCI** As our continuous integration platform we use CircleCI.



<u>Other tools:</u>

**Jira** All software development and maintenance issues/tickets are handled with Jira. This means that we log bugs, feature requests, epics and user stories in this system. This tools is mainly used by software engineer and product owners.

**ZenDesk** Our service desk ticketing system is ZenDesk. This tool is mainly used by service desk colleagues.

**Nmbrs** Our financial administration service provider uses Nmbrs to communicate salary specifications, leave days and cost declarations. Everybody will get an account through [Office Management]()



## Company wide hardware tools

**Laptop** To be able to work remote most employees will get a laptop to work with. The type of laptop can depend on the role you have. A software engineer might need to have a more powerful laptop than somebody why is mainly doing administrative work.

**Desktop computer** In the office a desktop computer might be an option instead of a laptop.

**Accessories** everybody can/will get the necessary items to support his workspace.

	* Mouse:  
	* Keyboard:
	* Laptop stand:
	* Screen(s): 
	* 



## Other useful tools

The above mentioned tools are the tools we use and support from Curious side. There are many more tools that might be helpful and useful to do what you need to do. Keep in mind that we try to align tools if it affects more than the individual user. 

**Balsamiq** Tool to design wireframes for software and websites

**Google Drive**

**Draw.io**

