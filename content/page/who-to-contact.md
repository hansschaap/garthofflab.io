---
title: Who to contact
type: docs
---
# Who to contact 

## HR

For all HR related questions you can contact HR ( hr@curious-inc.com ).

You can think of:

* Leave days

* Sickness

* Work environment and colleagues

* Study/ Courses / Education

* Salary (payment)

* Reimbursements

* Pension

* Business travel insurance

* Employment contracts

* Internships

  

## Office

For all office (including remote office) questions you can contact office management  ( om@curious-inc.com ).

You can think of:

* Pens and paper
* Chairs and other desk accessories
* Access key
* Parking
* Transportation (e.g. public transport or company car)
* Office lunch, coffee/tea and other kitchen stock



## IT

We have different IT issue related to our company IT tools. 

Questions regarding:

* Email accounts
* VPN accounts
* Microsoft Office (incl Outlook)
* Office IT
* Laptop  / Desktop and accessories
* Office printer

Can be sent to office-it@curious-inc.com


Questions regarding:

* Google Cloud / Azure / AWS
* Fundaments/Oxilion
* CircleCI 
* GitHub
* Rollbar

Can be sent to dev-it@curious-inc.com



## Finance & Legal

Questions about company financial issues (not being employees personal related finances like salary) like:

* Bills for products
* Bills for services
* Legal questions (e.g. NDAs)

Can be sent to finance@curious-inc.com 



## Co-owners

You can contact the co-owners through

h.schaap@curious-inc.com

w.eijkelenkamp@dation.nl



