# Passwords and general security guidelines

## Password security

1. Use a password manager and create a strong , random password  (strongly recommended)
2. Don't share your password with anyone else. (mandatory)
3. Use two-factor authentication for every service that support it. (mandatory)



## LastPass: Sharing Passwords and Sensitive Information

Sometimes you have to share passwords and/or sensitive information. We use [LastPass](https://www.lastpass.com/) to share this information. The features of the unpaid plan are:

- Access on multiple devices
- Multi-Factor Authentication
- Browser Extension with Autofill
- Password revocation
- Sharing of hidden passwords
- Multi-password/Vault sharing

If you need a password/credential in the future, please sign up using your using your @curious-inc email address. Premium plans are available for reimbursement on a need-based case.

## YubiKeys for 2FA

A YubiKey is a physical authentication token that can be used for two-factor authentication instead of using a smartphone authenticator app. Using a physical token is generally more secure. We especially recommend using a YubiKey if you have administrative access. Especially for accounts on GCP and AWS, since these credentials are particularly juicy targets.

Our recommended YubiKey models support several protocols. They support TOTP, which means you can use it anywhere you would use Google Authenticator; and U2F, which is a newer protocol specifically for physical security keys.

To use a YubiKey, you plug it in to your computer and launch the Yubico Authenticator app; it will then behave similarly to Google Authenticator. However, the cryptographic secrets used to generate your 2FA codes will not leave the YubiKey.

Certain services, such as Google, also support the U2F protocol. This lets you [use a security key directly](https://support.google.com/accounts/answer/6103523?hl=en&co=GENIE.Platform%3DDesktop&oco=0) as a second factor for login, without having to type a 2FA code.

##### Picking a YubiKey

If you already have a previous-generation YubiKey and you're happy with it, feel free to keep using it. If you're getting a new one, you can expense it.

The latest model is the the YubiKey 5, which comes in several form factors: See https://www.yubico.com/store and look at the Yubikey 5 series (not the Security Key Series). Any YubiKey 5 should work; pick one based on which ports/devices you might use it with. Here are some specific notes:

- YubiKey 5 NFC: supports USB-A and NFC. Currently, the NFC functionality only works properly on Android. iOS 13 is greatly expanding NFC capabilities, so proper support on iOS might show up later.
- YubiKey 5C: supports USB-C only.
- YubiKey 5Ci: This was just released as of August 2019. It has USB-C and Lightning connectors, and supposedly there will be an iOS version of Yubico Authenticator.

## Other security guidelines

It is good practice to only use root accounts when you need root privileges. In all other situations use a normal user account.

We will try to limit access to systems to a need to know/access basis. This to prevent the access to systems one doesn't need to have with related security risks and to comply to regulations and certifications.

Everybody who has a key to the office building will need to inform Office Management whenever he/she notices that the key is lost. This will prevent unauthorized access to our office.

Never share (potential) sensitive information with people (outside) the company who don't need to know about it.



