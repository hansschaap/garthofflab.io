# Country specific rules and policies

This section will describe the specific rules and policies for employees from other countries. Every country might have its own laws and national habits which we as a company want and need to support.

For every exception it should be clear if it is in *addition* to the general company rules and policies or that it is *instead of* company rules and policies.



## United States

### National Holidays

- New Year's Day - January 1
- Dr. Martin Luther King Jr.’s Birthday (Third Monday of January)
- President’s Day (Third Monday in February)
- Memorial Day (Last Monday in May)
- July 4
- Labor Day (First Monday in September)
- National Election Day - November 3, 2020
- Thanksgiving and the following day (Fourth Thursday and Friday in November)
- Christmas Day - December 25

Since the number of National Holidays in the USA or 4 days more than our basis (the Dutch Nation Holidays) we will compensate them by reducing the vacation days of US employees from 25 to 21 days per year.

