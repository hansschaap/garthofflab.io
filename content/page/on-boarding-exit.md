# On boarding and exit process 

Every time an employee starts or stops working for Curious we need to arrange several things. The paragraphs will list the actions to execute and a hind for when in the timeline it should be initiated (keeping in mind the lead time).

The lists can be used as checklist.

## On boarding a new employee

**Before start date**

* Contract
* Tax form
* Copy ID/Passport
* Person details



**On start date**

* Laptop/Desktop + accessories
* Accounts (O365)
* Screens
* VPN 
* Accounts ( Gitlab, Zendesk, Jira, Salesforce, )



**After start date**

* Pension forms
* Nmbrs account
* 



## Exit of an employee

**Before end date**

**On end date**

**After end date**

