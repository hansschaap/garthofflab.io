---
title: Time off
type: docs
---
# Time off

There is time to work and time to do other things. If you want to take time of for whatever reason you need to arrange this with your team (and/or office management).

Free days need to be requested through NMBRS. In this system you can also see your balance of free hours for the rest of the year.

## Official holidays (Dutch national holidays)

For all employees with a Dutch residence the following national holidays which are free days. They are not deducted from your vacation days. 

* New Year's Day - January 1 
* Easter Monday - (in 2021 this is ??)
* Ascension Day - (in 2021 this is ??)
* Pentecost Monday - (in 2021 this is ??)
* Christmas - December 25 and 26

As we know that non-Dutch residents can have other national holidays we will add those specific situations in the future when needed.

## Vacation Days

Vacation days are extremely important for everybody. Each employee will get 25 vacations days per year (if you start during a year you will get vacation days accordingly).

## Parental Leave

The Dutch law describes the options for parental leave. If you want to make use of this option please contact Office Management for more information.

## Sick days

If you fall sick and are not able to work you need to call in sick. In the morning of the day you cannot work because of sickness you let Office Management know before 9 a.m. that you are sick. On the day you resume working you let Office Management know before 9 a.m. that you are better again.

## Other leave situations

There are several situations which we want to specify including the rules regarding free days.

* Funeral
* Marriage
* Medical appointment
* Care for a sick child

## Special situations

The above mentioned situations are the common and less common situations for people. We also understand that there are situations which are more special. If you want any leave which is different than the situations mentioned above please contact Office Management and they will see what the next steps are.