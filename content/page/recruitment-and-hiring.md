# Recruitment & Hiring

The most up-to-date vacancies can be found at https://jobs.curious-inc.com. 

Any questions can be asked to Office Management.



## Employee Referrals

We encourage current employees to recommend people they know for open positions. If somebody knows a good candidate we are happy to prioritize their application to ensure it's reviewed quickly.

Please know that Curious does not have a formal referral program or incentives for employees to recommend candidates. We also do not automatically grant interviews to candidates recommended by current or former employees.