---
title: How to contribute to the handbook
type: docs
---
# How to contribute to the company handbook

## Introduction

The Curious Inc B.V. handbook is the source for our truth if it comes to company policy. This means that all employees should have access to it. If information is not correct, not clear or if information is missing, we want every employee to take action and (propose to) fix it.



## The contribution

It would be very nice if your proposal only needs to be approved and no extra work is needed. This means that your (merge/pull request) proposal is the text which can be put online without any editing.

It is of course possible to contribute in a less perfect way but his will make other people more busy than they might need to be with your contribution. Of course if your writing skills are not so good (yet) it is very fine. It is always better to contribute than to not contribute.

Keep in mind:

1. Write your proposal in your best possible way
2. Reread your proposal (test it ;-)  ) to prevent silly errors
3. ...

## HUGO 

We use [Hugo](https://gohugo.io) as our website building framework, its basis is Markdown for layout. Content management specifics can be found on their [website](https://gohugo.io/content-management/).



## The Git process

We use Git to keep track of the proposals and all changes of the handbook. GitLab is used to deploy all changes automatically to the internet, after a change request is accepted.

For a tutorial  on the way Git works you can go to [Git tutorial]().