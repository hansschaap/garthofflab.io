---
title: Curious Inc B.V. - The Handbook
type: docs
---

# Welcome to the Curious Inc B.V. handbook

It doesn't matter if you are a new employee, a student doing an assignment or already long-time company employee, this handbook will answer all your questions about how we do things in Curious Inc. This of course includes all of its related companies and teams like Dation, YesHugo and Grybb.

The content of this handbook will probably never be final. So if you miss things or have questions please ask them or propose changes and additions to this handbook directly.

The goals is that we will be able to say: **if it is not in the handbook then it's not true**.

We are a handbook first company (in progress) this means that we want to document our rules and regulation and decisions in a handbook. This handbook is available and accessible and  for all employees and all employees can [contribute]](/page/contribute-to-handbook) to it.. 

## Getting started 

* What to do on your [first day](/page/first-day-on-the-job) working for the company.
* Our [company values](/page/company-values) to show the foundation values the company stands for.
* [Working remote](/page/work-remote) the rules and guidelines which will help you to work remote.
* [Working in the office](/page/work-in-office), for those who work there every day and those who be there once in a while.
* Our [tools](/page/tools-we-use/) to support us doing the things we need to do.
* Some of our [benefits](/page/)

## Editing this handbook

This handbook is created with markdown as the 'styling language' see the [cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) and we store it a versioning system GitLab.
The handbook.curious-inc.com representation is generated from the version in our versioning system. This means that after a change is stored in GitLab it will automatically be uploaded (deployed) to the website (it might take some time however). 



## Contact information

**Curious Inc B.V.** 
Spoorstraat 114 - Floor 8
7551 CA  Hengelo (Ov.)
The Netherlands
Phone: +31 85 208 5205

More contact [details]()
